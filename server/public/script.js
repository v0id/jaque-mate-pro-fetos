const $ = selector => document.querySelector(selector);

let app = {
    identity: null,
};

const generatorEl = $('#generator');
const infoEl = $('#info');
const loadingScreenEl = $('#loading-screen');
const genNewIdentityBtnEl = $('#gen-new-identity-btn');
const getAddressBtnEl = $('#get-address-btn');

// TODO: hacer ajax antes del onload, y despues aplicar al DOM
window.onload = () => {
    Promise.all([
        genNewIdentity()
            .then(refreshData),
        fetch('/info.md')
            .then(res => res.text())
            .then(md => {
                infoEl.innerHTML = (new showdown.Converter()).makeHtml(md);
            }),
    ])
        .finally(() => loadingScreenEl.remove());
};

function toast (message) {
    let toastListEl = document.getElementById('toast-list');
    if (!toastListEl) {
        toastListEl = document.createElement('div');
        toastListEl.id = 'toast-list';
        document.body.appendChild(toastListEl);
    }
    
    const toastEl = document.createElement('div');
    toastEl.classList.add('toast');
    toastEl.textContent = message;

    const removeToast = () => {
        toastEl.style.opacity = 0;
        setTimeout(() => toastEl.remove(), 500);
    }

    const closeBtnEl = document.createElement('span');
    closeBtnEl.classList.add('toast-close-btn');
    closeBtnEl.onclick = removeToast;
    toastEl.appendChild(closeBtnEl);
    
    toastListEl.appendChild(toastEl);

    return toastEl;
}

getAddressBtnEl.onclick = event => {
    event.preventDefault();
    refreshData(true);
    return false;
};

genNewIdentityBtnEl.onclick = event => {
    event.preventDefault();
    genNewIdentity().then(refreshData);
    return false;
};

function loading (promise) {
    generatorEl.classList.add('loading');
    return promise.finally(() => {
        generatorEl.classList.remove('loading');
    });
}

let genIdentityPromise;

function genNewIdentity () {
    if (genIdentityPromise) return genIdentityPromise;
    return genIdentityPromise = loading(
        fetch('/newRandomIdentity')
    )
        .then(res => res.json())
        .then(data => {
            app.identity = data.identity;
        })
        .finally(() => {
            genIdentityPromise = undefined;
        });
}

let refreshPromise;

function refreshData (wantAddress = false) {
    if (refreshPromise) return refreshPromise;
    if (!generatorEl.addressId.value && wantAddress) {
        return toast('necesitas una ID para conseguir una dirección!');
    }
    return refreshPromise = loading(
        fetch('/getData', {
            method: 'POST',
            body: JSON.stringify({
                identity: app.identity,
                id: generatorEl.addressId.value,
            }),
            headers: { 'Content-Type': 'application/json' },
        })
    )
        .then(res => res.json())
        .then(data => {
            console.log(data)
            if (data.error) {
                switch (data.error) {
                    case 'noIdentity':
                        toast('error raro! por favor contactar a @v0idifier :)');
                        break;
                    case 'addressNotFound':
                        toast('dirección no encontrada! quizás copiaste mal la id =)');
                        break;
                    default:
                        toast(`error desconocido! ${data.error}`);
                }
                return;
            }
            if (data.address) generatorEl.address.value = data.address;
            generatorEl.identityName.value = data.name;
            generatorEl.identityEmail.value = data.email;
        })
        .finally(() => {
            refreshPromise = undefined;
        });
}