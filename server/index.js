const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const { MongoClient } = require('mongodb');
const app = express();

const url = process.env.MONGO_URL;
const dbName = process.env.MONGO_DB_NAME;

let dbClient = new MongoClient();

// const data =
//     require('../data/conseguirData.js')
//         (path.join(__dirname, '../data/direcciones-latest.json'));
const lib = require('../lib/jqpf-data');

app.use(express.static(path.join(__dirname, './public')));
app.use(bodyParser.json())


// const randomItem = arr => arr[Math.floor(Math.random() * arr.length)];

// console.log(data)

// function getData () {
//     return {
//         address: randomItem(data).DISPLAYNAME,
//     };
// }

// app.get('/data', (req, res) => {
//     res.send(getData());
// });

app.get('/name', async (req, res) => {
    const c = dbClient.db(dbName).collection('nombres');

    const item = c.aggregate([{ $sample: { size: 1 } }]);
    res.send((await item.toArray())[0]);
});

app.get('/newRandomIdentity', async (req, res) => {
    const identity = await lib.Identity.random({ db: dbClient.db(dbName) });
    let obj = {};
    obj.identity = identity;
    res.send(obj);
});

app.post('/getData', (req, res) => {
    console.log(req.body)
    if (!req.body.identity) {
        return res.status(400).send({ error: 'noIdentity' });
    }
    // if (!req.body.id) {
    //     return res.status(400).send({ error: 'noId' });
    // }
    const identity = new lib.Identity(req.body.identity);
    const addressObj = lib.getAddressObj(req.body.id);
    if (!addressObj && req.body.id) {
        return res.status(404).send({ error: 'addressNotFound' });
    }
    res.send({
        address: addressObj && identity.generateAddress(addressObj),
        name: identity.generateName(),
        email: identity.generateEmail(),
    });
})

const PORT = process.env.PORT || 3000;

MongoClient.connect(url, { useNewUrlParser: true })
    .then(client => {
        dbClient = client;
        app.listen(PORT, () => {
            console.log(`listening at http://localhost:${PORT}`);
        });
    })
    .catch(err => {
        console.error('mongodb error')
        console.error(err);
        process.exit(1);
    });