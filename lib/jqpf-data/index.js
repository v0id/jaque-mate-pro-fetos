const path = require('path');

const data =
    require('../../data/conseguirData.js')
        (path.join(__dirname, '../../data/direcciones-latest.json'));

const random = require('./random.js');

function pipify (func, name, obj) {
    // console.log(func,name,obj)
    // obj[name] = ;
    return { ...obj, [name]: (...args) => {
        return func(obj, ...args);
    }};
}
function pipeIfElse (obj, conditionFunc, ifFunc, elseFunc) {
    if (conditionFunc(obj)) {
        return ifFunc(obj);
    } else {
        return elseFunc(obj);
    }
}

let mod = {};

mod.Case = {
    Normal: 'normal',
    Upper: 'upper',
    Lower: 'lower',
};

mod.SpacesUsage = {
    None: 0,
    One: 1,
    Two: 2,
}

mod.CommaUsage = {
    None: 0,
    Normal: 1,
};

mod.NameType = {
    LastNameBeforeWithComma: 0,
    FirstNameFirst: 1,
};

mod.EmailProvider = {
    HotmailComAr: 0,
    HotmailCom: 1,
    Gmail: 2,
    YahooCom: 3,
    YahooComAr: 4,
    FibertelComAr: 5,
};

mod.EmailType = {
    FirstLast: 0,
    LastFirst: 1,
    FirstDotLast: 2,
    LastDotFirst: 3,
};

mod.SpaceBeforeCommaUsage = mod.SpacesUsage;

mod.stringToCase = (string, ccase) => {
    switch (ccase) {
        case mod.Case.Normal: return string;
        case mod.Case.Lower: return string.toLowerCase();
        case mod.Case.Upper: return string.toUpperCase();
    }
};
mod.multiplyString = (string, number) => {
    let str = '';
    for (let i = 0; i < number; i++) {
        str += string;
    }
    return str;
};
mod.numberToSpaces = number => mod.multiplyString(' ', number);
mod.numberToCommas = number => mod.multiplyString(',', number);

mod.randomOption = (
    defaultValue,
    values,
    defaultOptionChance = .97,
) => random.itemCustom(
    [defaultValue, defaultOptionChance],
    ...Object.values(values)
        .filter(val => val !== defaultValue)
        .map(
            (val, i, arr) => [
                val,
                (1 - defaultOptionChance) / arr.length
            ],
        ),
)

mod.randomCase = (string, ccase) => {
    const finalCase = mod.randomOption(ccase, mod.Case);
    return mod.stringToCase(string, finalCase);
};
mod.randomCommas = commaUsage => {
    const finalCommaUsage = mod.randomOption(commaUsage, mod.CommaUsage, .97);
    return mod.numberToCommas(finalCommaUsage);
};
mod.randomSpaces = spaceUsage => {
    const finalSpaceUsage = mod.randomOption(spaceUsage, mod.SpacesUsage, .97);
    return mod.numberToSpaces(finalSpaceUsage);
};

mod.Identity = class Identity {
    constructor (config = {}) {
        if (config.name === undefined) throw new Error('no name in config');
        if (config.nameType === undefined) throw new Error('no nameType in config');
        if (config.emailType === undefined) throw new Error('no emailType in config');
        if (config.emailNumbers === undefined) throw new Error('no emailNumbers in config');
        if (config.emailProvider === undefined) throw new Error('no emailProvider in config');
        if (config.case === undefined) throw new Error('no case in config');
        if (config.spacesUsage === undefined) throw new Error('no spacesUsage in config');
        if (config.commaUsage === undefined) throw new Error('no commaUsage in config');
        if (config.spaceBeforeCommaUsage === undefined) throw new Error('no spaceBeforeCommaUsage in config');
        
        this.name = config.name;
        this.nameType = config.nameType;
        this.emailType = config.emailType;
        this.emailNumbers = config.emailNumbers;
        this.emailProvider = config.emailProvider;
        this.spaceBetweenName = config.spaceBetweenName;
        this.case = config.case;
        this.spacesUsage = config.spacesUsage;
        this.commaUsage = config.commaUsage;
        this.spaceBeforeCommaUsage = config.spaceBeforeCommaUsage;
    }
    generateAddress (addressObj) {
        let string = '';
        console.log(addressObj)
        string += mod.randomCase(addressObj.road, this.case);
        string += ' ';
        string += addressObj.houseNumber;
        string += mod.randomSpaces(this.spaceBeforeCommaUsage);
        string += mod.randomCommas(this.commaUsage);
        string += mod.randomSpaces(this.spacesUsage);
        string += addressObj.city.map(
            (cityStr, i, { length }) =>
                mod.randomCase(cityStr, this.case)
                + (i !== length - 1 ?
                    mod.randomSpaces(this.spaceBeforeCommaUsage)
                    + mod.randomCommas(this.commaUsage)
                    + mod.randomSpaces(this.spacesUsage)
                : ''),
        ).join('');
    
        return string;
    }
    generateRandomAddress () {
        const addressObj = random.itemInArray(data);
        return this.generateAddress(addressObj);
    }
    generateName () {
        return pipify(pipeIfElse, 'ifElse', mod
            .stringToCase(this.name, this.case))
            .ifElse(
                obj => this.nameType === mod.NameType.FirstNameFirst,
                obj => obj
                    .split(', ')
                    .reverse()
                    .join(' '),
                obj => obj
                    .split(', ')
                    .join(',' + mod.randomSpaces(this.spacesUsage)),
            )
            .split(' ')
            .join(mod.numberToSpaces(this.spaceBetweenName));
    }
    generateEmail () {
        let string = '';
        const firstNameArr = this.name.split(', ');
        switch (this.emailType) {
            case mod.EmailType.FirstLast:
                if (firstNameArr.length > 1) {
                    string += firstNameArr[1];
                    string += firstNameArr[0];
                } else {
                    //assume [first name] [...last name]
                    string += firstNameArr[0].split(' ')[0];
                    string += firstNameArr[0].split(' ').slice(1).join('');
                }
                break;
            case mod.EmailType.LastFirst:
                if (firstNameArr.length > 1) {
                    string += firstNameArr[0];
                    string += firstNameArr[1];
                } else {
                    //fuck it, act as FirstLast
                    string += firstNameArr[0];
                }
                break;
            case mod.EmailType.FirstDotLast:
                if (firstNameArr.length > 1) {
                    string += firstNameArr[1];
                    string += '.';
                    string += firstNameArr[0];
                } else {
                    //assume [first name] [...last name]
                    string += firstNameArr[0].split(' ')[0];
                    string += '.';
                    string += firstNameArr[0].split(' ').slice(1).join('');
                }
                break;
            case mod.EmailType.LastDotFirst:
                if (firstNameArr.length > 1) {
                    string += firstNameArr[0];
                    string += '.';
                    string += firstNameArr[1];
                } else {
                    //fuck it, act as FirstLast
                    //assume [first name] [...last name]
                    string += firstNameArr[0].split(' ')[0];
                    string += '.';
                    string += firstNameArr[0].split(' ').slice(1).join('');
                }
                break;
        }
        string += this.emailNumbers;
        string += '@';
        switch (this.emailProvider) {
            case mod.EmailProvider.FibertelComAr: string += 'fibertel.com.ar'; break;
            case mod.EmailProvider.Gmail: string += 'gmail.com'; break;
            case mod.EmailProvider.HotmailCom: string += 'hotmail.com'; break;
            case mod.EmailProvider.HotmailComAr: string += 'hotmail.com.ar'; break;
            case mod.EmailProvider.YahooCom: string += 'yahoo.com'; break;
            case mod.EmailProvider.YahooComAr: string += 'yahoo.com.ar'; break;
        }
        string = string.split(' ').join('');
        string = mod.stringToCase(string, this.case);
        return string;
    }
    // generateData (addressObj) {
    //     return {
    //         address: addressObj 
    //             ? this.generateAddress(addressObj)
    //             : this.generateRandomAddress(),
    //         name: this.generateName(),
    //     };
    // }
};

// function obj2Name ({ apellidoNombre: nombre }, { nameType,}) {
//     switch (nameType) {
//         let array = nombre.split(', ');
//         for (const last of array.slice(0, array.length - 1)) {
//             array.push(last);
//             array.splice(0, 1);
//         }
//         return array.join(' ');
//     }
// }

mod.Identity.random = async ({ db }) => {
    const nameObj = (await db
        .collection('nombres')
        .aggregate([{ $sample: { size: 1 } }])
        .toArray())[0];
    let config = {
        name: nameObj.apellidoNombre,
        nameType: random.itemCustom(
            [mod.NameType.FirstNameFirst, 0.95],
            [mod.NameType.LastNameBeforeWithComma, 0.05],
        ),
        emailProvider: random.itemCustom(
            [mod.EmailProvider.YahooComAr, 0.25],
            [mod.EmailProvider.YahooCom, 0.05],
            [mod.EmailProvider.FibertelComAr, 0.15],
            [mod.EmailProvider.Gmail, 0.15],
            [mod.EmailProvider.HotmailComAr, 0.25],
            [mod.EmailProvider.HotmailCom, 0.15],
        ),
        emailType: random.itemCustom(
            [mod.EmailType.FirstLast, 0.35],
            [mod.EmailType.LastFirst, 0.25],
            [mod.EmailType.FirstDotLast, 0.25],
            [mod.EmailType.LastDotFirst, 0.15],
        ),
        emailNumbers: random.int(0, 9999),
        spaceBetweenName: random.itemCustom(
            [mod.SpacesUsage.One, .75],
            [mod.SpacesUsage.Two, .25],
        ),
        case: random.itemCustom(
            [mod.Case.Normal, .5],
            [mod.Case.Upper, .3],
            [mod.Case.Lower, .2],
        ),
        spacesUsage: random.itemCustom(
            [mod.SpacesUsage.None, .2],
            [mod.SpacesUsage.One, .75],
            [mod.SpacesUsage.Two, .05],
        ),
        commaUsage: random.itemCustom(
            // [mod.CommaUsage.None, .15],
            // [mod.CommaUsage.Normal, .85],
            [mod.CommaUsage.Normal, 1],
        ),
        spaceBeforeCommaUsage: random.itemCustom(
            [mod.SpaceBeforeCommaUsage.None, .9],
            [mod.SpaceBeforeCommaUsage.One, .07],
            [mod.SpaceBeforeCommaUsage.Two, .03],
        ),
    };

    // config.name = test(nameObj, config);
    return new mod.Identity(config);
};
// console.log(data)
mod.getAddressObj = id1 => data.find(({ id: id2 }) => id1 == id2);

module.exports = mod;