const fs = require('fs');
const path = require('path');
const assert = require('assert');
const MongoClient = require('mongodb').MongoClient;

const url = process.env.MONGO_URL;
const dbName = process.env.MONGO_DB_NAME;

const fileString = fs.readFileSync(
    path.join(__dirname,
        '../data/inutil o sources/nombres/igj-autoridades-genero-2018-07.csv'
    )).toString();

const lines = fileString.split('\n');

const keys = lines[0].slice(1, lines[0].length - 1).split('","');
console.log(url,dbName)
new MongoClient(url, {
    useNewUrlParser: true,
}).connect(async function (err, client) {
    assert.equal(null, err);
    console.log("Connected successfully to server");

    const db = client.db(dbName);
    const collection = db.collection('nombres');

    for (let line of lines.splice(1)) {
        
        line = line.trim();
        line = line.slice(1, line.length - 1);
        line = line.split('","');
        let el = {};
        for (const [index, key] of keys.entries()) {
            el[key] = line[index];
        }
        el.numeroCorrelativo = el['"numero_correlativo'];
        delete el['"numero_correlativo'];
        el.apellidoNombre = el.apellido_nombre;
        delete el.apellido_nombre;
        el.tipoDocumento = el.descripcion_tipo_documento;
        delete el.descripcion_tipo_documento;
        el.numDocumento = el.numero_documento;
        delete el.numero_documento;
        el.genero = el.genero_autoridad;
        delete el.genero_autoridad;
        el.tipoSocietario = el['descripcion_tipo_societario"'];
        delete el['descripcion_tipo_societario"'];
        // console.log(el)
        await collection.insertOne(el);
        console.log(`inserted ${el.apellidoNombre}`)
    }

    client.close();
});

// let output = raw
//     .map(el => {
//         let out = {};
//         for (const [index, key] of keys.entries()) {
//             out[key] = el[index];
//         }
//         return out;
//     })
//     .map(el => {
//         el.numeroCorrelativo = el.numero_correlativo;
//         delete el.numero_correlativo;
//         el.apellidoNombre = el.apellido_nombre;
//         delete el.apellido_nombre;
//         el.tipoDocumento = el.descripcion_tipo_documento;
//         delete el.descripcion_tipo_documento;
//         el.numDocumento = el.numero_documento;
//         delete el.numero_documento;
//         el.genero = el.genero_autoridad;
//         delete el.genero_autoridad;
//         el.tipoSocietario = el.descripcion_tipo_societario;
//         delete el.descripcion_tipo_societario;
//         return el;
//     });